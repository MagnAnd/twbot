from view.gui import GUI

def main():
    gui = GUI()
    gui.mainloop()

def run():
    main()

if __name__ == "__main__":
    run()
