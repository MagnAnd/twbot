import http.cookiejar as cookiejar
import urllib.request 
import urllib
import requests

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException

from model.offstrats import OffensiveStrategy
from model.village import Village

import threading
import time


class WebInteracter(object):
    #TODO Move much of the village specific to the village class
    def __init__(self, login, password):
        """ Start up... """
        self.login = login
        self.password = password


        self.__loginToTribalWars()
        self.__read_construction_levels()
        #TODO read building queue in the beginning
        self.village = self.__construct_village()

        self.update_threads = list()
        self.barrack_threads = list()
        self.stable_threads = list()

        upgrade_thread = threading.Thread(target=self.main_loop)
        upgrade_thread.start()
        upgrade_thread.join()

    def main_loop(self):
        while True:
            # self.__troop_stuff()
            self.village.update()
            print(self.village.building_queue)
            self.__upgrade_village(self.village)
            time.sleep(10)

    def __visit_headquarter(self):
        hq_url = "&screen=main"
        self.driver.get(self.base_url+hq_url)

    def __read_construction_levels(self):
        self.__visit_headquarter()
        
        prefix_id = "main_buildrow_"
        
        self.buildings = {
                "main": 0,
                "place": 0,
                "statue": 0,
                "wood": 0,
                "stone": 0,
                "iron": 0,
                "farm": 0,
                "storage": 0,
                "barracks": 0,
                "stables": 0,
                "smith": 0,
                "hide": 0,
                "garage": 0,
                "watchtower": 0,
                "market": 0,
                "snob": 0, # Academy
                }

        tmp = self.driver.find_elements_by_xpath("//*[contains(@id,'%s')]" % prefix_id)
        for l in tmp:
            # Extract the name of the construction
            # E.g main_buildrow_main, we want main
            building = l.get_attribute('id').split("_")[2]

            span = l.find_element_by_tag_name('span')
            level_string = span.text.split()[1]
            if level_string.isdigit():
                level = int(level_string)
            else:
                level = 0
            print(building, level)
            self.buildings[building] = level

    def __construct_village(self):
        village = Village(self.buildings)
        strat = OffensiveStrategy()
        village.inject_strategy(strat)
        return village

    def __can_enqueue(self):
        if len(self.update_threads) > 0:
            return False
        return True
    
    def __has_enough_resources(self, building):
        tmp = self.driver.find_element_by_id("main_buildrow_"+building[0])
        cost_wood = int(tmp.find_element_by_class_name("cost_wood").text)
        cost_stone = int(tmp.find_element_by_class_name("cost_stone").text)
        cost_iron = int(tmp.find_element_by_class_name("cost_iron").text)
        
        total_wood = int(self.driver.find_element_by_id("wood").text)
        total_stone = int(self.driver.find_element_by_id("stone").text) # Not used
        total_iron = int(self.driver.find_element_by_id("iron").text)

        return cost_wood <= total_wood and cost_stone <= total_wood \
                and cost_iron <= total_iron


    def __update_level(self, *building, **kwargs):
        print(building, kwargs)
        self.buildings[building[0]] = building[1]+1
        self.update_threads.pop()
        print("Upgrade", building[0], "to", str(building[1]+1), "done")

    def __upgrade_village(self, village):
        self.__visit_headquarter()
        if self.__can_enqueue() and self.__has_enough_resources(village.building_queue[0]):
            building = village.building_queue.pop()
            self.__visit_headquarter()
            tmp = self.driver.find_element_by_id("main_buildrow_"+building[0])
            btn = tmp.find_element_by_class_name("btn-build")
            time_field = tmp.find_element_by_class_name("time").find_element_by_xpath("..")
            time_text = time_field.text
            time = time_text.split(':')

            seconds = int(time[0])*3600 + int(time[1])*60 + int(time[2])
            btn.click()

            update_level_thread = threading.Timer(seconds, self.__update_level, args=building)
            update_level_thread.start()
            self.update_threads.append(update_level_thread)
        self.__troop_stuff()

            
            
            #village.buildings[building[0]] = building[1]+1

    def __has_barracks(self):
        self.__visit_headquarter()
        if self.buildings["barracks"] > 0:
            return True
        return False

    def __visit_barracks(self):
        barracks_url = "&screen=barracks"
        self.driver.get(self.base_url+barracks_url)

    def __can_enqueue_troops(self):
        if len(self.barrack_threads) > 0:
            return False
        return True
        # Unsure if this works 100 % since no waiting between 156-157
        # try:
        #     tmp = self.driver.find_element_by_class_name("current_prod_wrapper")
        #     return True
        # except NoSuchElementException:
        #     return False

    def __enqueue_troops(self):
        unit = self.village.unit_queue.pop()
        print(unit)
        try:
            elem = self.driver.find_element_by_id(unit[0])
            btn = self.driver.find_element_by_class_name("btn-recruit")
        except NoSuchElementException:
            # self.driver.find_element_by_id(unit[0] + "_afford_hint")
            return False
        
        elem.clear()
        elem.send_keys(unit[1])

        time = self.driver.find_element_by_id(unit[0] + "_cost_time")
        print(time)

        barrack_thread = threading.Timer(time,self.__print_unit)
        barrack_thread.start()
        self.barrack_threads.append(barrack_thread)

        btn.click()

    def __print_unit(self):
        print(Done)

    def __troop_stuff(self):
        if self.__can_enqueue_troops() & self.__has_barracks():
            self.__visit_barracks()
            self.__enqueue_troops()
        else:
            return False
    

    def __loginToTribalWars(self):
        """
        Handle login. 
        """

        #TODO Remove
        with open("../../twlogin.txt", 'r') as f:
            self.login = f.readline().strip()
            self.password = f.readline().strip()

        prefix_world = "/page/play/en"
        world = "98"

        url = 'https://www.tribalwars.net/'
        
        driver = webdriver.Firefox()
        driver.get(url)
        user = driver.find_element_by_id("user")
        user.send_keys(self.login)
        password = driver.find_element_by_id("password")
        password.send_keys(self.password)
        password.submit()

        WebDriverWait(driver, 10).until(EC.presence_of_element_located(\
                (By.CLASS_NAME, "world_button_active")))
        driver.find_element_by_class_name("world_button_active").click()
        # TODO Handle if it does not popup
        try:
             driver.find_element_by_class_name("popup_box_close").click()
        except NoSuchElementException:
            pass

        self.base_url = driver.current_url
        self.driver = driver

