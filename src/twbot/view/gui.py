import tkinter as tk
from utils.webinteracter import WebInteracter

class GUI(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.pack()
        self.create_widgets()

    def login(self):
        user = self.username.get()
        pw = self.password.get()
        self.web = WebInteracter(user, pw)
        print(user, pw)

    def create_widgets(self):
        self.username = tk.Entry()
        self.username.pack()
        self.password = tk.Entry()
        self.password.config(show="*")
        self.password.pack()
        self.submit = tk.Button(text="Log in", command=self.login).pack()

