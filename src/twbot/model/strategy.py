from abc import ABCMeta, abstractmethod
class Strategy(object):
    __metaclass__ = ABCMeta


    def __init__(self):
        self.update_order = [
                ('wood', 1),
                ('stone', 1),
                ('iron', 1),
                ('wood', 2),
                ('stone', 2),
                ('iron', 2),
                ('statue', 1),
                ('main', 3),
                ('barracks', 1),
                ('farm', 1),
                ('main', 5),
                ('smith', 2),
                ('wood', 3),
                ('stone', 3),
                ('iron', 3),
                ('barracks', 5),
                ('main', 10),
                ('smith', 5),
                ('stables', 3),
                ('market', 1),
                ('farm', 5),
                ('wood', 4),
                ('stone', 4),
                ('iron', 4),
                ('wood', 5),
                ('stone', 5),
                ('iron', 5),
                ('wood', 6),
                ('stone', 6),
                ('iron', 6),
                ('farm', 10),
                ('wood', 7),
                ('stone', 7),
                ('iron', 7),
                ('wood', 8),
                ('stone', 8),
                ('iron', 8),
                ('wood', 9),
                ('stone', 9),
                ('iron', 9),
                ('wood', 10),
                ('stone', 10),
                ('iron', 10),
                ('farm', 13),
                ('smith', 10),
                ('garage', 2),
                ('smith', 12),
                ('farm', 20),
                ('main', 23),
                ('stables', 10),
                ('barracks', 10),
                ('stables', 15),
                ('barracks', 20),
                ('warehouse', 24),
                ('farm', 29),
                ('smith', 20),
                ('snob', 1),
                ('market', 10),
                ('stables', 20),
                ('barracks', 25),
                ('wall', 20),
                ]
    @abstractmethod
    def get_unit_to_train(self):
        pass

    def get_building_to_update(self, buildings):
        for item in self.update_order:
            if buildings[item[0]] < item[1]:
                return (item[0], buildings[item[0]])
        return None


