class Village(object):
    
    def __init__(self, building_levels: dict):
        self.buildings = building_levels
        self.building_queue = []
        self.unit_queue = []

    def inject_strategy(self, strat):
        self.strategy = strat

    def update(self):
        unit = self.strategy.get_unit_to_train()
        building = self.strategy.get_building_to_update(self.buildings)
        self.__enqueue(unit, building)

    def __enqueue(self, unit, building):
        # TODO Look at requiriments and make decision before
        # blindly enqueueing

        self.unit_queue.append(unit)

        if building not in self.building_queue:
            self.building_queue.append(building)





